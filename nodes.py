"""
Defines the nodes for the decision tree.
"""

class Node(object):
	def __init__(self, category, parent, left, right):
		self.category = category
		self.parent = parent
		self.left = left
		self.right = right
	
